<?php

namespace Ajanssens;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;

class OutdatedWithJsonExportPlugin implements PluginInterface, Capable
{

    /**
     * @inheritdoc
     */
    public function activate(Composer $composer, IOInterface $io)
    {
    }

    /**
     * @inheritdoc
     */
    public function getCapabilities()
    {
        return array(
            'Composer\Plugin\Capability\CommandProvider' => 'Ajanssens\CommandProvider'
        );
    }
}
