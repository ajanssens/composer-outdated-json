<?php

namespace Ajanssens;

use Ajanssens\Command\OutdatedWithJsonExportCommand;
use Composer\Plugin\Capability\CommandProvider as CommandProviderCapability;

class CommandProvider implements CommandProviderCapability
{
    public function getCommands()
    {
        return array(new OutdatedWithJsonExportCommand());
    }
}
