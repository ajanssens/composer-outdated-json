<?php

namespace Ajanssens\Command;

use Composer\Command\OutdatedCommand;
use Composer\Composer;
use Composer\Package\BasePackage;
use Composer\Package\PackageInterface;
use Composer\Package\Version\VersionSelector;
use Composer\Repository\CompositeRepository;
use Composer\DependencyResolver\Pool;
use Composer\Repository\PlatformRepository;
use Composer\Semver\Semver;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class OutdatedWithJsonExportCommand extends OutdatedCommand
{
    /** @var Pool */
    private $pool;

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this->setName('outdated-export-json');
        $this->addArgument('file', InputOption::VALUE_REQUIRED, 'Export result in specified file as JSON');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(!$input->getArgument('file')) {
            $output->writeln('<error>Please provide a filename</error>');

            return;
        }

        $composer = $this->getComposer(false);
        $platformOverrides = array();
        if ($composer) {
            $platformOverrides = $composer->getConfig()->get('platform') ?: array();
        }
        $platformRepo = new PlatformRepository(array(), $platformOverrides);
        $phpVersion = $platformRepo->findPackage('php', '*')->getVersion();
        $repos = array($this->getComposer()->getRepositoryManager()->getLocalRepository());

        $packages = array();

        foreach ($repos as $repo) {
            /** @var PackageInterface $package */
            foreach ($repo->getPackages() as $package) {
                $latestPackage = $this->findLatestPackage($package, $composer, $phpVersion);
                $packages[$package->getPrettyName()] = array(
                    'current' => $package->getFullPrettyVersion(),
                    'latest' => $latestPackage->getFullPrettyVersion(),
                    'status' => 'orange'
                );

                $constraint = $package->getVersion();
                if (0 !== strpos($constraint, 'dev-')) {
                    $constraint = '^'.$constraint;
                }

                if ($latestPackage && $latestPackage->getFullPrettyVersion() === $package->getFullPrettyVersion()) {
                    $packages[$package->getPrettyName()]['status'] = 'green';
                }

                if ($latestPackage && $latestPackage->getVersion() && Semver::satisfies($latestPackage->getVersion(), $constraint)) {
                    $packages[$package->getPrettyName()]['status'] = 'red';
                }
            }
        }

        $fs = new Filesystem();
        $fs->dumpFile($input->getArgument('file'), json_encode($packages));
        $output->writeln($input->getArgument('file').' written with packages versions');
    }

    /**
     * Given a package, this finds the latest package matching it
     *
     * @param  PackageInterface $package
     * @param  Composer         $composer
     * @param  string           $phpVersion
     *
     * @return PackageInterface|null
     */
    private function findLatestPackage(PackageInterface $package, Composer $composer, $phpVersion)
    {
        // find the latest version allowed in this pool
        $name = $package->getName();
        $versionSelector = new VersionSelector($this->getPool($composer));
        $stability = $composer->getPackage()->getMinimumStability();
        $flags = $composer->getPackage()->getStabilityFlags();
        if (isset($flags[$name])) {
            $stability = array_search($flags[$name], BasePackage::$stabilities, true);
        }

        $bestStability = $stability;
        if ($composer->getPackage()->getPreferStable()) {
            $bestStability = $package->getStability();
        }

        $targetVersion = null;
        if (0 === strpos($package->getVersion(), 'dev-')) {
            $targetVersion = $package->getVersion();
        }

        return $versionSelector->findBestCandidate($name, $targetVersion, $phpVersion, $bestStability);
    }

    /**
     * @param Composer $composer
     * @return Pool
     */
    private function getPool(Composer $composer)
    {
        if (!$this->pool) {
            $this->pool = new Pool($composer->getPackage()->getMinimumStability(), $composer->getPackage()->getStabilityFlags());
            $this->pool->addRepository(new CompositeRepository($composer->getRepositoryManager()->getRepositories()));
        }

        return $this->pool;
    }
}
